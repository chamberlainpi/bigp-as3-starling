package starling.display {
	import com.bigp.utils.DictionaryUtils;
	import flash.utils.Dictionary;
	import starling.animation.Juggler;
	import starling.core.Starling;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.textures.Texture;

	/**
	 * ...
	 * @author Pierre Chamberlain
	 */
	public class MovieClipMulti extends Sprite {
		
		private var _helperTextures:Vector.<Texture>;
		private var _movieclips:Vector.<MovieClip>;
		private var _movieclipsByName:Dictionary;
		private var _currentMovieClip:MovieClip;
		private var _currentStateName:String;
		private var _defaultFPS:Number = 12;
		
		public function MovieClipMulti(pDefaultFPS:Number=-1) {
			super();
			
			_movieclips = new Vector.<MovieClip>();
			_movieclipsByName = new Dictionary();
			_helperTextures = new Vector.<Texture>();
			
			if (pDefaultFPS >= 0) {
				_defaultFPS = pDefaultFPS;
			}
		}
		
		public override function dispose():void {
			if (_movieclips) {
				inline_removeCurrentMC();
				
				for (var i:int=_movieclips.length; --i>=0;) {
					var mc:MovieClip = _movieclips[i];
					mc.dispose();
				}
				
				_movieclips.length = 0;
				_movieclips = null;
				
				DictionaryUtils.destroyAll(_movieclipsByName);
				_movieclipsByName = null;
				
				_helperTextures.length = 0;
				_helperTextures = null;
				
				_currentMovieClip = null;
				_currentStateName = null;
			}
			
			super.dispose();
		}
		
		public function addState( pStateName:String, pFPS:Number, pTextures:Vector.<Texture>, pLoop:Boolean=true, pPlayNow:Boolean=false ):void {
			var mc:MovieClip = new MovieClip(pTextures, pFPS);
			mc.loop = pLoop;
			mc.name = pStateName;
			mc.fps = pFPS;
			_movieclipsByName[pStateName] = mc;
			_movieclips[_movieclips.length] = mc;
			
			if (pPlayNow) {
				playMovieClip(mc, false);
			}
		}
		
		public function addStates( pTexturesMaster:Vector.<Texture>, pStates:Array, pFilterPrefixes:Boolean=false ):void {
			var theTextures:Vector.<Texture>;
			
			for (var i:int=0, iLen:int=pStates.length; i<iLen; i++) {
				var stateArr:Array = pStates[i];
				var theFPS:Number, theFrames:Array, theStateName:String, theLoop:Boolean, thePlayNow:Boolean;
				var theMasterTextures:Vector.<Texture> = pTexturesMaster;
				
				switch (stateArr.length) {
					case 2:
						if(stateArr[0] is Array) {
							theFrames = stateArr[0];
							theStateName = stateArr[1];
						} else {
							theStateName = stateArr[0];
							theFrames = stateArr[1];
						}
						
						theFPS = _defaultFPS;
						theLoop = true;
						thePlayNow = false;
						break;
						
					case 3:
						if(stateArr[0] is Array) {
							theFrames = stateArr[0];
							theStateName = stateArr[1];
						} else {
							theStateName = stateArr[0];
							theFrames = stateArr[1];
						}
						
						if (stateArr[2] is Boolean) {
							theFPS = _defaultFPS;
							theLoop = stateArr[2];
						} else if (stateArr[2] is Number) {
							theFPS = stateArr[2];
							theLoop = true;
						} else {
							throw new Error("Unhandled Movieclip State parameter [#2]:\n" + stateArr.join(", "));
						}
						
						thePlayNow = false;
						break;
						
					case 4:
						theStateName = stateArr[0];
						theFrames = stateArr[1];
						theFPS = stateArr[2];
						theLoop = stateArr[3];
						thePlayNow = false;
						break;
					case 5:
						theStateName = stateArr[0];
						theFrames = stateArr[1];
						theFPS = stateArr[2];
						theLoop = stateArr[3];
						thePlayNow = stateArr[4];
						break;
						
					default: throw new Error("Unhandled number of arguments in state:\n" + stateArr.join(", "));
						
				}
				
				if (pFilterPrefixes) {
					_helperTextures.length = 0;
					
					for (var k:int=0, kLen:int=pTexturesMaster.length; k<kLen; k++) {
						var filteredTexture:Texture = pTexturesMaster[k];
						if (filteredTexture.name.indexOf(theStateName) > -1) {
							_helperTextures[_helperTextures.length] = filteredTexture;
						}
					}
					
					theMasterTextures = _helperTextures;
				}
				
				//Create a new list of textures to add to the MovieClip:
				theTextures = new Vector.<Texture>();
				
				for (var j:int=0, jLen:int=theFrames.length; j<jLen; j++) {
					var frameNum:int = theFrames[j];
					if (frameNum >= theMasterTextures.length) {
						throw new Error("Incorrect frame # passed, there is only " + theMasterTextures.length + " textures.");
					}
					
					theTextures[theTextures.length] = theMasterTextures[frameNum]
				}
				
				//Add the new state:
				addState(theStateName, theFPS, theTextures, theLoop, thePlayNow);
				theTextures = null;
			}
			
			theTextures = null;
		}
		
		private function playMovieClip(pMC:MovieClip, pForced:Boolean, pLoop:int = -1):void {
			if (_currentMovieClip == pMC && !pForced) return;
			
			if(_currentMovieClip!=pMC) {
				inline_removeCurrentMC();
			}
			
			_currentMovieClip = pMC;
			
			if (!_currentMovieClip) return;
			
			_currentStateName = pMC.name;
			
			if (pLoop===0) pMC.loop = false;
			else if (pLoop > 0) pMC.loop = true;
			
			if (pForced) {
				_currentMovieClip.currentFrame = 0;
			}
			
			if (_currentMovieClip.parent !== this) {
				addChild(_currentMovieClip);
				juggler.add(_currentMovieClip);
			}
			
			_currentMovieClip.play();
		}
		
		public function pause():void {
			if (!_currentMovieClip) return;
			_currentMovieClip.pause();
		}
		
		public function stop():void {
			if (!_currentMovieClip) return;
			_currentMovieClip.stop();
		}
		
		public function play():void {
			if (!_currentMovieClip) return;
			_currentMovieClip.play();
		}
		
		public function playState(pStateName:String, pForced:Boolean = false):Boolean {
			pStateName = pStateName.toLowerCase();
			var mc:MovieClip = _movieclipsByName[pStateName];
			if (!mc) throw new Error("No such state: " + pStateName);
			if(_currentMovieClip===mc && !pForced) return false;
			
			playMovieClip(mc, pForced);
			return true;
		}
		
		[Inline]
		private final function inline_removeCurrentMC():void {
			//Remove any current movie-clips:
			if (_currentMovieClip) {
				_currentMovieClip.stop();
				removeChild(_currentMovieClip);
				juggler.remove( _currentMovieClip );
				_currentMovieClip = null;
				_currentStateName = null;
			}
		}
		
		public static function get juggler():Juggler { return Starling.juggler; }
		
		public function get currentMovieClip():MovieClip { return _currentMovieClip; }
		public function get currentStateName():String { return _currentStateName; }
		public function get fps():Number { return _currentMovieClip.fps; }
		public function set fps(n:Number):void { _currentMovieClip.fps = n; }
		
		
	}
}